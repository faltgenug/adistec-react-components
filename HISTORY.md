### version: "3.4.0" by Federico Altgenug faltgenug@adistec.com ###
* added customFunction filter
### version: "3.3.20" by fregnier fregnier@adistec.com ###
* Not filter when filter option is empty
### version: "3.3.18" by fregnier fregnier@adistec.com ###
* Add peerDepsExternal plugin
### version: "3.3.17" by fregnier fregnier@adistec.com ###
* Moved dependencies to peerDependencies and remove unnecesary dependencies
### version: "3.3.16" by fregnier fregnier@adistec.com ###
* Add filterOption to Autocomplete
### version: "3.3.15" by fregnier fregnier@adistec.com ###
* Add onClick to Drawer items
### version: "3.3.13" by fregnier fregnier@adistec.com ###
* Added default sort to Table Component
### version: "3.3.12" by jbosnic jbosnic@adistec.com ###
* Added Subheader functionality to Menu Component
### version: "3.3.11" by nrossello nrossello@adistec.com ###
* Import React Router in package.json
### version: "3.3.10" by nrossello nrossello@adistec.com ###
* Added onInputChange method to Autocomplete component
### version: "3.3.9" by nrossello nrossello@adistec.com ###
* Added customLabel function to Breadcrumb component
### version: "3.3.8" by nrossello nrossello@adistec.com ###
* Added property disabled to Autocomplete component
### version: "3.3.7" by nrossello nrossello@adistec.com ###
* Fix error Autocomplete Component.
### version: "3.3.6" by nrossello nrossello@adistec.com ###
* Fix error onChange method in Autocomplete.
### version: "3.3.5" by nrossello nrossello@adistec.com ###
* Add onChange method to Autocomplete.
### version: "3.3.4" by nrossello nrossello@adistec.com ###
* Fix onChange method in DatePickerWithInput and TextField component
### version: "3.3.3" by nrossello nrossello@adistec.com ###
* Add onChange Method to DatePickerWithInput component
### version: "3.3.2" by nrossello nrossello@adistec.com ###
* Inicialize Kendo Components
### version: "3.3.1" by Nrossello nrossello@adistec.com ###
* Added onClick, onChange and MaxLenght properties to TextField component
### version: "3.3.0" by nrossello nrossello@adistec.com ###
* Add new component: SortableComponent
### version: "3.2.7" by nrossello nrossello@adistec.com ###
* Fix Error in doubleClickFunction in table
### version: "3.2.6" by nrossello nrossello@adistec.com ###
* Add props trDoubleClickFunction in table
### version: "3.2.5" by nrossello nrossello@adistec.com ###
* Changes in image upload in Upload File Component
### version: "3.2.4" by Nrossello nrossello@adistec.com ###
* Fix Error UploadFile Component
### version: "3.2.3" by nrossello nrossello@adistec.com ###
* Fix Error required validations method
### version: "3.2.2" by nrossello nrossello@adistec.com ###
* Fix error default date
### version: "3.2.1" by nrossello nrossello@adistec.com ###
* Add property defaultDate to DatePicker Component
### version: "3.2.0" by nrossello nrossello@adistec.com ###
* Add PrivateRoute Component
### version: "3.1.5" by Elad Haim eladhaim22@gmail.com ###
* UploadFile: spliting component and adding validations
* SelectField: adding validtaions
### version: "3.1.4" by Juan Bosnic jbosnic@adistec.com ###
* SelectField:Fixes when value seted after component is mounted
### version: "3.1.3" by Juan Bosnic jbosnic@adistec.com ###
* Added floating label styles
### version: "3.1.2" by Elad Haim eladhaim22@gmail.com ###
* UploadFile: bug fixes
### version: "3.1.1" by Elad Haim eladhaim22@gmail.com ###
* SelectField: Adding support of autocomplete
* UploadFile:Adding support of aysnc validation
* 
### version: "3.1.0" by nrossello nrossello@adistec.com ###
* Added new component: Banner
### version: "3.0.3" by Elad Haim eladhaim22@gmail.com ###
* UploadFile: fixing upload file for multiple false option
### version: "3.0.1" by Federico Altgenug faltgenug@adistec.com ###
* Filtros de fecha solamente aceptan fechas en formato ISO8601 o Date
### version: "3.0.0" by Federico Altgenug faltgenug@adistec.com ###
* Uso de React Router v4
### version: "2.0.17" by Elad Haim eladhaim22@gmail.com ###
* DatePickerWithInput: adding locale support
### version: "2.0.16" by Juan Bosnic jbosnic@adistec.com ###
* Reviewed styles for Export to Excel button
### version: "2.0.15" by Juan Bosnic jbosnic@adistec.com ###
* Reviewed styles for Export to Excel button
### version: "2.0.14" by Elad Haim eladhaim22@gmail.com ###
* Fixing UploadFile after reset bug
### version: "2.0.13" by Juan Bosnic jbosnic@adistec.com ###
* AdistecUploadFile supports multiple files, with correct styles and messages
* Header supports white background color by props
### version: "2.0.12" by Elad Haim eladhaim22@gmail.com ###
* Fixes in Upload multiple files
### version: "2.0.11" by Elad Haim eladhaim22@gmail.com ###
* Adding supports of multiple files uploading
### version: "2.0.10" by Juan Bosnic jbosnic@adistec.com ###
* Corrected UploadFile behaviour on Firefox
### version: "2.0.9" by Elad Haim eladhaim22@gmail.com ###
* Adding disableAutocomplete property to filters component
### version: "2.0.8" by Elad Haim eladhaim22@gmail.com ###
* Modifies modal property in Modal
### version: "2.0.7" by Elad Haim eladhaim22@gmail.com ###
* Fixes in Modal
### version: "2.0.6" by Elad Haim eladhaim22@gmail.com ###
* Adding onRequestClose to Modal
### version: "2.0.5" by nrossello nrossello@adistec.com ###
* Fix error en Filters Component
### version: "2.0.4" by Elad Haim eladhaim22@gmail.com ###
* Adding property bodyClass to Modal
### version: "2.0.3" by nrossello nrossello@adistec.com ###
* Fix errors in styles of AdistecTable Component
### version: "2.0.2" by nrossello nrossello@adistec.com ###
* Fix Error array data type in Filters Component
### version: "2.0.1" by nrossello nrossello@adistec.com ###
* Added Hide / Show Buttons in Filters Component
### version: "2.0.0" by Elad Haim eladhaim22@gmail.com ###
* Cleaning unnecessary properties from filters config object
* Adding support of multiple fuse filters in filters component
### version: "1.4.14" by nrossello nrossello@adistec.com ###
* Fix Errors in Filters Components
### version: "1.4.13" by nrossello nrossello@adistec.com ###
* Add Validate props to AdistecUploadFile and clickable props to Adistec Table
### version: "1.4.12" by Elad Haim eladhaim22@gmail.com ###
* Filters: Fix search on selectfield multiline empty array
* Warning fixes
### version: "1.4.11" by nrossello nrossello@adistec.com ###
* Changes in the error label in AdistecUploadFile component.
### version: "1.4.10" by nrossello nrossello@adistec.com ###
* Add styles in rejected image error (Adistec Upload File)
### version: "1.4.9" by Elad Haim eladhaim22@gmail.com ###
* Table: adding afterSort handler
* UploadFile: change user interface messages
### version: "1.4.8" by nrossello nrossello@adistec.com ###
* Fix Errors in Styles
### version: "1.4.7" by nrossello nrossello@adistec.com ###
* Fix Error in Header
### version: "1.4.6" by nrossello nrossello@adistec.com ###
* Fix Error in Header
### version: "1.4.5" by Elad Haim eladhaim22@gmail.com ###
* Table: fixing bug
### version: "1.4.4" by Elad Haim eladhaim22@gmail.com ###
* Adding support of custom accessors in sortable
### version: "1.4.3" by Elad Haim eladhaim22@gmail.com ###
* Fixes orderable table
### version: "1.4.2" by Elad Haim eladhaim22@gmail.com ###
* Adding sortable option to table columns
### version: "1.4.1" by nrossello nrossello@adistec.com ###
* Fix styles in Adistec Filters
### version: "1.4.0" by nrossello nrossello@adistec.com ###
* Aggregation of Breadcrumb component and corrections of errors in the AutoComplete component
### version: "1.3.1" by nrossello nrossello@adistec.com ###
* Fix Syntax Errors and add optional login and logout button to header and optional drawer
### version: "1.3.0" by Elad Haim eladhaim22@gmail.com ###
* Adding IntlProvider component
* Bug and warning fixes
### version: "1.2.7" by Elad Haim eladhaim22@gmail.com ###
* PasswordField fixes
* rollup.config fixes
* Style duplications fixes
### version: "1.2.6" by Elad Haim eladhaim22@gmail.com ###
* Fixes in i18n
* Fixes rollup configuration
### version: "1.2.5" by nrossello nrossello@adistec.com ###
* Add Component AdistecPasswordField and NetsuitePasswordField
### version: "1.2.4" by Juan Bosnic jbosnic@adistec.com ###
* Added NetsuitePassword component, with visual fixes and translations.
### version: "1.2.3" by Eugenio González Pernía egonzalez@adistec.com ###
* Modify header logo to get src from prop, optional username and email.
### version: "1.2.2" by Elad Haim eladhaim22@gmail.com ###
* Adding a style bundle
### version: "1.2.1" by Elad Haim eladhaim22@gmail.com ###
* Adding type attribute to text field
### version: "1.2.0" by Juan Bosnic jbosnic@adistec.com ###
* Added new components (Nav, SingleSelectionList), new container (IntranetForm), new theme (theme_modern)
### version: "1.1.0" by Juan Bosnic jbosnic@adistec.com ###
* Added new components (Nav, SingleSelectionList), new container (IntranetForm), new theme (theme_modern)
### version: "1.0.2" by Elad Haim eladhaim22@gmail.com ###
* Adding global dependencies to peerDependencies
### version: "1.0.1" by Elad Haim eladhaim22@gmail.com ###
* Changing library name
### version: "1.0.0" by Elad Haim eladhaim22@gmail.com ###
* Adistec react components initial version
