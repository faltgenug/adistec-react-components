import React, { PropTypes } from 'react';
import { IndexRedirect, Route, Router } from 'react-router';
import Main from '../containers/Main.jsx';


export default (
    <Router>
        <Route path="/" component={Main}/>
    </Router>
);
