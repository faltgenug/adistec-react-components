import React from 'react';
import  Toggle  from 'redux-form-material-ui/es/Toggle';
import { Field } from 'redux-form';

const propTypes = {
    defaultChecked: React.PropTypes.bool,
    marginTop:React.PropTypes.string,
    name: React.PropTypes.string,
    size:React.PropTypes.string,
    title: React.PropTypes.string,
    titlePosition:React.PropTypes.string
};

const AdistecSwitch = ({
    name,
    title,
    defaultChecked,
    titlePosition,
    size,
    marginTop
}) => (
    <div className={size ? 'col s' + size : ''}  style={{ marginTop: marginTop }}>
        <Field  defaultToggled={defaultChecked}
                label={title}
                component={Toggle}
                name={name}
                labelPosition={titlePosition}/>
    </div>
);

AdistecSwitch.propTypes = propTypes;

export default AdistecSwitch;