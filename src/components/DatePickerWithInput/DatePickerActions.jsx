import React from 'react';
import styled from 'styled-components';

const propTypes = {
    openDialog: React.PropTypes.func,
    resetValue: React.PropTypes.func
};

const StyledSpan = styled.span`
    margin-top: 29px;
    height: 50%;
    position: absolute;
    right:10px;
    top:0px; 
    color:#CCC; 
    font-size:12px;
    transition: all 150ms cubic-bezier(0.23, 1, 0.32, 1) 0ms !important;
    
    i{    
        &:hover{
            color:#005FAB;    	
            cursor:pointer;
        }
        
        &+i{
           margin-left:5px;
        }
    }    
`;

const DatePickerActions = ({ resetValue,openDialog, clearButton }) => {
	return (<StyledSpan>
        {clearButton ? <i className="fa fa-times" aria-hidden="true" onClick={resetValue} /> : null}
        <i className="fa fa-calendar" onClick={openDialog}/>
    </StyledSpan>);
};

DatePickerActions.propTypes = propTypes;

export default DatePickerActions;