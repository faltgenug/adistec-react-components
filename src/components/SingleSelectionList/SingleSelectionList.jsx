import React from 'react';
import { Field } from 'redux-form';
import './SingleSelectionList.scss';

const propTypes = {
    column:React.PropTypes.array,
    disabled:React.PropTypes.bool,
    list: React.PropTypes.array,
    name:React.PropTypes.string,
    validate: React.PropTypes.array
};

const RadioButtomGroup = (props) => {
    let numOfCols = props.columns ? 'a-list--columns_' + props.columns : null;

    const change = (e) => {
        props.input.onChange(e.target.value.toString());
    };

    return (<ul className={'single-selection-list ' + numOfCols}>
        {props.list.map((element,index) =>
            (<li key={index} data-state={element.name.toLowerCase().replace(/\s/g,'')}>
                <input type="radio" onChange={change} value={element.id.toString()}
                       id={`request${index}`} name={props.input.name}
                       checked={props.input.value == element.id.toString() ? true : false} disabled={props.disabled ? true : false}/>
                <label htmlFor={`request${index}`} >
                    {element.icon ? <div className="item-icon">{element.icon}</div> : null}
                    <div className="item-text">{element.name}</div>
                </label>
            </li>)
        )}
        {props.meta.submitFailed && props.meta.invalid ? <error>Debe elegir un Estado</error> : null}
    </ul>);
};

const SingleSelectionList = ({ list,name,validate,disabled,columns }) => (
    <div>
        <Field component={RadioButtomGroup} validate={validate} list={list} name={name} disabled={disabled} columns={columns} />
    </div>
);

SingleSelectionList.propTypes = propTypes;

export default SingleSelectionList;