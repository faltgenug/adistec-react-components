import React from 'react';
import { Field } from 'redux-form';
import './Nav.scss';

const NavItems = (props) => {
    return (<ul className="a-nav a-nav--section">
        <li><a href="#">Accesos</a>
            <ul className="a-list--columns_2">
                <li><a href="#">Adistec Webmail</a></li>
                <li><a href="#">NetSuite Login</a></li>
            </ul>
        </li>
        <li><a href="#">Recursos gráficos</a>
            <ul className="a-list--columns_2">
                <li><a href="#">Logotipo Corporativo</a></li>
                <li><a href="#">Tipografías Corporativas</a></li>
                <li><a href="#">Hojas Membretadas (para todas las subsidiarias)</a></li>
                <li><a href="#">Hoja Membretada Adistec Education</a></li>
                <li><a href="#">Hoja Membretada APS</a></li>
                <li><a href="#">Hoja Membretada ACS</a></li>
                <li><a href="#">Presentación Corporativa - Enero 2017 - Español e Inglés</a></li>
            </ul>
        </li>
        <li><a href="#">Adistec Enterprise Cloud</a>
            <ul className="a-list--columns_2">
                <li><a href="#">AEC Password Policy (English)</a></li>
                <li><a href="#">AEC Password Policy Signed Copy (English)</a></li>
                <li><a href="#">AEC Backup Information Policy (English)</a></li>
                <li><a href="#">AEC Backup Information Policy Signed Copy (English)</a></li>
            </ul>
        </li>
        <li><a href="#">Mapa de Empleados</a></li>
        <li className="right"><a href="#" className="a-btn a-btn--border a-btn--pill a-btn_md a-btn_strong a-btn_white">Recomienda a un amigo</a></li>
    </ul>);
};

const propTypes = {
    disabled: React.PropTypes.bool,
    list: React.PropTypes.array,
    name: React.PropTypes.string,
    validate: React.PropTypes.func
};


const Nav = ({ list,name,validate,disabled }) => (
    <div>
        <Field component={NavItems} validate={validate} list={list} name={name} disabled={disabled}/>
    </div>
);

Nav.propTypes = propTypes;

export default Nav;