import React from 'react';
import Slider from 'react-slick';
import './Banner.scss';
import './slick/slick.scss';
import './slick/slick-theme.scss';


const propTypes = {
    items: React.PropTypes.array.isRequired,
    setting: React.PropTypes.object
};

const defaultProps = {
    setting: {
        autoplay: true,
        autoplaySpeed: 3000,
        dots: true,
        arrows:false,
        infinite: true,
        speed: 500,
        slidesToShow: 1
    },
};

class Banner extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
          return (
            this.props.items ?
              <div className="a--banner_container col s12">
                 <Slider {...this.props.setting}>
                     {this.props.items}
                </Slider>
              </div>
            : null
        );
    }
}

Banner.propTypes = propTypes;
Banner.defaultProps = defaultProps;

export default Banner;