import en from './i18n/en/index.json';
import es from './i18n/es/index.json';
import pt from './i18n/pt/index.json';
import merge from 'lodash/merge';

export const mergeTranslations = (externalTranslations) => {
  return merge(translations,externalTranslations);
};

let translations = {
  en: en,
  es: es,
  pt: pt
};

export const translatedMessages = (locale,messages) => {
  return flattenMessages(messages[locale]);
};

const flattenMessages = ((nestedMessages, prefix = '') => {
  if (nestedMessages === null) {
    return {};
  }
  return Object.keys(nestedMessages).reduce((messages, key) => {
    const value       = nestedMessages[key];
    const prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === 'string') {
      Object.assign(messages, { [prefixedKey]: value });
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey));
    }

    return messages;
  }, {});
});

